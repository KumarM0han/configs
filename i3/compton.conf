backend = "glx";
paint-on-overlay = true;
glx-no-stencil = true;
vsync = "opengl-swc";

# Shadow
shadow = true; # Enabled client-side shadows on windows.
no-dock-shadow = true; # Avoid drawing shadows on dock/panel windows.
no-dnd-shadow = true; # Don't draw shadows on DND windows.
clear-shadow = true; # Zero the part of the shadow's mask behind the window (experimental).
shadow-radius = 7; # The blur radius for shadows. (default 12)
shadow-offset-x = -7; # The left offset for shadows. (default -15)
shadow-offset-y = -7; # The top offset for shadows. (default -15)
shadow-exclude = [
  "n:e:Notification",
  "n:e:Docky",
  "g:e:Synapse",
  "g:e:Conky",
  "n:w:*Firefox*",
  "n:w:*Chromium*",
  "n:w:*dockbarx*",
  "class_g ?= 'Cairo-dock'",
  "class_g ?= 'Xfce4-notifyd'",
  "class_g ?= 'Xfce4-power-manager'",
  "class_g ?= 'i3status'"
];

# Opacity
detect-client-opacity = true;

# Fading
fading = true; # Fade windows during opacity changes.
fade-delta = 4; # The time between steps in a fade in milliseconds. (default 10).
fade-in-step = 0.03; # Opacity change between steps while fading in. (default 0.028).
fade-out-step = 0.03; # Opacity change between steps while fading out. (default 0.03).
#no-fading-openclose = true; # Fade windows in/out when opening/closing

# Window type settings
wintypes:
{
  tooltip = { fade = true; shadow = false; };
};

focus-exclude = [
  "class_g = 'i3status'",
  "class_g = 'Cairo-clock'",
  "class_g = 'Bar'",                    # lemonbar
  "class_g = 'slop'"                   # maim
];

opacity-rule = [
  "92:class_g     = 'i3status'",
  "80:class_g     = 'Bar'"
];
